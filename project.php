<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">

        <title>COLORS - responsive multifunctional colorful html template</title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta name="description" content="">
        <meta name="author" content="">

        <link href='http://fonts.googleapis.com/css?family=Arvo:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>



        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->


        <script src="<?php echo ipThemeUrl('js/jquery.js') ?>"></script>
        <script src="<?php echo ipThemeUrl('js/jquery-migrate.js') ?>"></script>



        <link href="<?php echo ipThemeUrl('css/bootstrap.css') ?>" rel="stylesheet">
        <link href="<?php echo ipThemeUrl('css/style.css') ?>" rel="stylesheet">
        <link href="<?php echo ipThemeUrl('css/media.css') ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo ipThemeUrl('css/font-awesome.css') ?>">
        <link href="<?php echo ipThemeUrl('css/bxslider.css') ?>" rel="stylesheet" />

    </head>
    <body>



        <div class="wrapper100percent additionalpage2wrapper">
            <div class="additionalpagewrapper">


                <div class="container">

                    <div class="row">
                        <div class="col-lg-8 additionalpagehome">
                            <a href="/">back to homepage</a>

                        </div>
                    </div>


                    <div class="row">
                        <div class="col-lg-8 col-sm-12 col-12">


                            <div class="bxwrapadditional">

                                <ul id="bxslider4">

                                    <li>

                                        <img src="<?php echo ipThemeUrl('images/additional1.jpg') ?>" alt="picture"/>
                                    </li>

                                    <li>

                                        <img src="<?php echo ipThemeUrl('images/additional2.jpg') ?>" alt="picture"/>
                                    </li>


                                </ul>
                            </div>


                        </div>



                        <div class="col-lg-4 col-sm-12 col-12 additionalpageright">

                            <h3>Project Red</h3>

                            <p>DATE: 12/12/2013</p>
                            <p>AUTHOR: John Doe</p>

                            <div class="additionallist">
                                <p><i class="icon-ok"></i> Nam liger tempor cum soluta nobis eleifent </p>

                                <p><i class="icon-ok"></i> Nam liger tempor cum soluta nobis eleifent </p>

                            </div>

                            <div class="additionallink">
                                <a href="">see more</a>
                            </div>

                        </div>

                    </div>


                    <div class="row">

                        <div class="col-lg-12 ">
                            <div class="additionalpage">

                                <h4>Project Red</h4>

                                <p class="additionalpagetext">
                                    Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores.


                                    Lorem ipsum dolor, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes.
                                </p>
                            </div>
                        </div>

                    </div>


                </div>


            </div>
        </div><!--/section2 end-->


        <script src="<?php echo ipThemeUrl('js/jquery.bxslider.js') ?>"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('#bxslider4').bxSlider();
            });
        </script>



    </body>
</html>



