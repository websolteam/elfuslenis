<?php echo ipView('_header.php')->render(); ?>




<div class="topwrapwrapper paralax1">

    <!--<div id="mask"></div>-->

    <section class="topwrap">

        <ul>
            <li>
                <div class="textbx">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-lg-offset-3 toplinkwrapper ">
                                <div class="yellowheader">
                                    <h2 class="title1">
                                        <?php
                                        echo ipSlot('text', array(
                                            'id' => 'Title1',
                                            'tag' => 'span',
                                            'default' => 'ELFŲ SLĖNIS',
                                            'class' => 'color2',
                                        ));
                                        ?>
                                    </h2>
                                    <h3 class="subtitle2">
                                        <?php
                                        echo ipSlot('text', array(
                                            'id' => 'SubTitle2',
                                            'default' => 'Vaikų laisvalaikio ir užimtumo centras',
                                            'class' => 'subtitle3',
                                        ));
                                        ?>
                                    </h3>
                                </div>

                                <div class="centered bottomicons">
                                    <a href="https://www.facebook.com/elfuslenis" target="blank"><i class="icon-facebook"></i></a>
                                </div>

                                <a href="#1">
                                    <div class="toplink">
                                        <i class="icon-chevron-down"></i>

                                    </div>
                                </a>

                            </div> <!--/col-lg-12 end-->
                        </div> <!--/row end-->
                    </div> <!--/container end-->
                </div> <!--/textbx end-->
            </li>
        </ul>
    </section>
</div><!--container end-->

<div id="cbp-so-scroller">

    <div class="wrapper100percent section3">

        <div id="1" class="sectionwrapper"></div>

        <div class="mainheadlinewrapper">
            <div class="mainheadline">
                <section class="cbp-so-section">
                    <article class="cbp-so-side2 cbp-so-side-left">
                        <h2>
                            <?php
                            echo ipSlot('text', array(
                                'id' => 'WhoText',
                                'tag' => 'div',
                                'default' => 'Mes esame',
                                'class' => ''
                            ));
                            ?>
                        </h2>
                    </article>
                    <article class="cbp-so-side2 cbp-so-side-right">
                        <h4>
                            <?php
                            echo ipSlot('text', array(
                                'id' => 'WhoWeAreText',
                                'default' => 'Vaikų laisvalaikio ir užimtumo centras',
                            ));
                            ?>
                        </h4>
                    </article>
                </section>
            </div>
        </div>

        <div class="container">
            <section class="cbp-so-section row">
                <article class="cbp-so-side cbp-so-side-left">
                    <div class="aboutusbackground">
                        <div class="aboutusbackgroundinner">
                            <h3>
                                <?php
                                echo ipSlot('text', array(
                                    'id' => 'Title1',
                                    'tag' => 'span',
                                    'default' => 'Elfų slėnis',
                                ));
                                ?>
                            </h3>
                            <i class="icon-cog smaller"> </i>
                            <p>
                                <?php
                                echo ipSlot('text', array(
                                    'id' => 'WhatWeDo',
                                    'tag' => 'span',
                                    'default' => 'tekstas1',
                                ));
                                ?>

                            </p>
                        </div>
                    </div>

                    <div class="aboutusbackground">
                        <div class="aboutusbackgroundinner">
                            <h3>
                                <?php
                                echo ipSlot('text', array(
                                    'id' => 'Title2',
                                    'tag' => 'span',
                                    'default' => 'Elfų mini darželis',
                                ));
                                ?>
                            </h3>
                            <i class="icon-rocket smaller"> </i>
                            <p>
                                <?php
                                echo ipSlot('text', array(
                                    'id' => 'Text2',
                                    'tag' => 'span',
                                    'default' => 'tekstas2',
                                ));
                                ?>
                            </p>
                        </div>
                    </div>
                </article>

                <article class="cbp-so-side cbp-so-side-right">
                    <div class="aboutusbackground">
                        <div class="aboutusbackgroundinner">
                            <h3>
                                <?php
                                echo ipSlot('text', array(
                                    'id' => 'Title3',
                                    'tag' => 'span',
                                    'default' => 'Gimtadienio šventės',
                                ));
                                ?>
                            </h3>
                            <i class="icon-gift smaller"> </i>
                            <p>

                                <?php
                                echo ipSlot('text', array(
                                    'id' => 'Text3',
                                    'tag' => 'span',
                                    'default' => 'tekstas3',
                                ));
                                ?>

                            </p>
                        </div>
                    </div>

                    <div class="aboutusbackground">
                        <div class="aboutusbackgroundinner">
                            <h3>
                                <?php
                                echo ipSlot('text', array(
                                    'id' => 'Title4',
                                    'tag' => 'span',
                                    'default' => 'Elfų mokyklėlė',
                                ));
                                ?>
                            </h3>
                            <i class="icon-pencil smaller"> </i>
                            <p>
                                <?php
                                echo ipSlot('text', array(
                                    'id' => 'Text4',
                                    'tag' => 'span',
                                    'default' => 'tekstas4',
                                ));
                                ?>
                            </p>
                        </div>
                    </div>
                </article>
            </section>
        </div>

        <div class="mainheadlinewrappersmall">
            <div class="mainheadlinesmall">
                <section class="cbp-so-section">
                    <article class="cbp-so-side2 cbp-so-side-left">
                        <h2>
                            <?php
                            echo ipSlot('text', array(
                                'id' => 'OurSuccess',
                                'default' => 'Ypatingas dėmesys',
                            ));
                            ?>
                        </h2>
                    </article>
                    <article class="cbp-so-side2 cbp-so-side-right">
                        <h4>
                            <?php
                            echo ipSlot('text', array(
                                'id' => 'OurSuccessText',
                                'default' => 'Vaikams ir tėveliams',
                            ));
                            ?>
                        </h4>
                    </article>
                </section>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <section class="cbp-so-section">
                    <article class="cbp-so-side  cbp-so-side-left">
                        <div class="col-lg-6 achievement">
                            <div class="achievementinner">
                                <h3>
                                    <?php
                                    echo ipSlot('text', array(
                                        'id' => 't123',
                                        'default' => '3-6',
                                    ));
                                    ?>
                                </h3>
                                <i class="icon-smile"></i>
                            </div>
                            <h5>
                                <?php
                                echo ipSlot('text', array(
                                    'id' => 'Text123',
                                    'default' => 'Vaikai',
                                ));
                                ?>
                            </h5>
                        </div>

                        <div class="col-lg-6 achievement">
                            <div class="achievementinner">
                                <h3>
                                    <?php
                                    echo ipSlot('text', array(
                                        'id' => 't456',
                                        'default' => '2',
                                    ));
                                    ?>
                                </h3>
                                <i class="icon-heart"></i>
                            </div>
                            <h5>
                                <?php
                                echo ipSlot('text', array(
                                    'id' => 'Text456',
                                    'default' => 'Pedagogės',
                                ));
                                ?>
                            </h5>
                        </div>
                    </article>

                    <article class="cbp-so-side  cbp-so-side-right">
                        <div class="col-lg-6 achievement">
                            <div class="achievementinner">
                                <h3>
                                    <?php
                                    echo ipSlot('text', array(
                                        'id' => 't789',
                                        'default' => '9-14',
                                    ));
                                    ?>
                                </h3>
                                <i class="icon-time"></i>
                            </div>
                            <h5>
                                <?php
                                echo ipSlot('text', array(
                                    'id' => 'Text789',
                                    'default' => 'Val.',
                                ));
                                ?>
                            </h5>
                        </div>

                        <div class="col-lg-6 achievement">
                            <div class="achievementinner">
                                <h3>
                                    <?php
                                    echo ipSlot('text', array(
                                        'id' => 't101',
                                        'default' => '5',
                                    ));
                                    ?>
                                </h3>
                                <i class="icon-sun"></i>
                            </div>
                            <h5>
                                <?php
                                echo ipSlot('text', array(
                                    'id' => 'Text101',
                                    'default' => 'Dienos',
                                ));
                                ?>
                            </h5>
                        </div>
                    </article>
                </section>
            </div>
        </div>

    </div><!--/section3 end-->

    <!--DIV270-->


    <div class="paralax4 section5">



        <section class="bxwrap2">


            <div class="mainheadlinewrapper">
                <div class="mainheadline">
                    <section class="cbp-so-section">
                        <article class="cbp-so-side2 cbp-so-side-left">
                            <h2>
                                <?php
                                echo ipSlot('text', array(
                                    'id' => 'Portfolio',
                                    'default' => 'Darbelių galerija',
                                ));
                                ?>
                            </h2>
                        </article>
                        <article class="cbp-so-side2 cbp-so-side-right">
                            <h4>
                                <?php
                                echo ipSlot('text', array(
                                    'id' => 'PortfolioText',
                                    'default' => 'Mūsų kūriniai',
                                ));
                                ?>
                            </h4>
                        </article>
                    </section>
                </div>
            </div>


            <div class="container">
                <div class="row">
                    <div class="col-lg-12 mtb20">
                        <div class="col-lg-3">
                            <?php echo ipBlock('gallery1')->render(); ?>
                        </div>
                        <div class="col-lg-3">
                            <?php echo ipBlock('gallery2')->render(); ?>
                        </div>
                        <div class="col-lg-3">
                            <?php echo ipBlock('gallery3')->render(); ?>
                        </div>
                        <div class="col-lg-3">
                            <?php echo ipBlock('gallery4')->render(); ?>
                        </div>

                    </div>
                </div>
            </div>

        </section>


    </div>


    <div class="footerbottom">
        <div id="6" class="sectionwrapper"></div>

        <div class="mainheadlinewrapper">
            <div class="mainheadline">
                <section class="cbp-so-section">
                    <article class="cbp-so-side2 cbp-so-side-left">
                        <h2>

                            <?php
                            echo ipSlot('text', array(
                                'id' => 'ContactUs',
                                'default' => 'Susisiekite',
                            ));
                            ?>
                        </h2>
                    </article>
                    <article class="cbp-so-side2 cbp-so-side-right">
                        <h4>

                            <?php
                            echo ipSlot('text', array(
                                'id' => 'ContactUstext',
                                'default' => 'Jei turite mums klausimų, parašykite',
                            ));
                            ?>
                        </h4>
                    </article>
                </section>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h5><i class="icon-envelope"></i>Jūsų žinutė...</h5>

                    <?php echo ipBlock('contactform')->render(); ?>

                    <!--/comment-form-->
                    <div>
                        <div class="row address">
                            <div class="col-lg-4">
                                <h6>Adresas:</h6>
                                <p>Trakų g. 31, Kaunas </p>
                            </div>
                            <div class="col-lg-4">
                                <h6>Telefonas:</h6>
                                <p>8 685 25224</p>
                            </div>
                            <div class="col-lg-4">
                                <h6>El. paštas:</h6>
                                <p><a href="mailto:info@elfuslenis.lt">info@elfuslenis.lt</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="paralax7">
        <div class="footerbottomblack">

            <div class="row">
                <div class="col-lg-6 copyright text-left">
                    <p> Elfų slėnis 2014<span>&copy; Sprendimas.<a href="http://www.websol.lt">WebSol.lt</a>.</span></p>
                </div>
                <div class="col-lg-6 text-right scrollbutton">  <!--/scrollbutton-->

                    <a href="javascript:scrollToTop()" title="go to top">Aukštyn<i class="icon-arrow-up"></i></a>
                </div><!--/scrollbutton end-->
            </div>

        </div>

    </footer>
    <!--/footerbottom end-->

</div><!--/cbp-so-scroller end-->
<?php echo ipView('_footer.php')->render(); ?>
