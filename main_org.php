<?php echo ipView('_header.php')->render(); ?>

<div class="wrapper100percent section1">

    <div id="1" class="sectionwrapper"></div>

    <div class="mainheadlinewrapper">
        <div class="mainheadline">
            <section class="cbp-so-section">
                <article class="cbp-so-side2 cbp-so-side-left">
                    <h2> Who<span> we are? </span></h2>
                </article>
                <article class="cbp-so-side2 cbp-so-side-right">
                    <h4>Lorem ipsum dolor ame lorem</h4>
                </article>
            </section>
        </div>
    </div>

    <div class="container">
        <section class="cbp-so-section row">
            <article class="cbp-so-side cbp-so-side-left">
                <div class="aboutusbackground">
                    <div class="aboutusbackgroundinner">
                        <h3><span>What we do </span></h3>
                        <i class="icon-briefcase smaller"> </i>
                        <p>
                            Namid quod mazim placerat. Typi non habent claritatem insitam; Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes lectores demonstraverunt lectores
                        </p>
                    </div>
                </div>

                <div class="aboutusbackground">
                    <div class="aboutusbackgroundinner">
                        <h3><span>Our Mision </span></h3>
                        <i class="icon-rocket smaller"> </i>
                        <p>
                            Nam doming id quod mazim. Typi non habent claritatem insitam; Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes lectores demonstraverunt lectores
                        </p>
                    </div>
                </div>
            </article>

            <article class="cbp-so-side cbp-so-side-right">
                <div class="aboutusbackground">
                    <div class="aboutusbackgroundinner">
                        <h3><span> Our Vision </span></h3>
                        <i class="icon-eye-open smaller"> </i>
                        <p>
                            Typi non habent claritatem insitam; Typi non habent claritatem insitam; Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes lectores demonstraverunt lectores
                        </p>
                    </div>
                </div>

                <div class="aboutusbackground">
                    <div class="aboutusbackgroundinner">
                        <h3><span> Organization </span></h3>
                        <i class="icon-cogs smaller"> </i>
                        <p>
                            Typi non habent claritatem insitam; Typi non habent claritatem insitam; Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes lectores demonstraverunt lectores
                        </p>
                    </div>
                </div>
            </article>
        </section>
    </div>

    <div class="mainheadlinewrappersmall">
        <div class="mainheadlinesmall">
            <section class="cbp-so-section">
                <article class="cbp-so-side2 cbp-so-side-left">
                    <h2> Our success </h2>
                </article>
                <article class="cbp-so-side2 cbp-so-side-right">
                    <h4>Lorem ipsum dolor sit amet lorem </h4>
                </article>
            </section>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <section class="cbp-so-section">
                <article class="cbp-so-side  cbp-so-side-left">
                    <div class="col-lg-6 achievement">
                        <div class="achievementinner">
                            <h3>324</h3>
                            <i class="icon-heart"></i>
                        </div>
                        <h5>Unique Projects</h5>
                    </div>

                    <div class="col-lg-6 achievement">
                        <div class="achievementinner">
                            <h3>485</h3>
                            <i class="icon-smile"></i>
                        </div>
                        <h5>Happy Customers</h5>
                    </div>
                </article>

                <article class="cbp-so-side  cbp-so-side-right">
                    <div class="col-lg-6 achievement">
                        <div class="achievementinner">
                            <h3>890</h3>
                            <i class="icon-trophy"></i>
                        </div>
                        <h5>Winner Awards</h5>
                    </div>

                    <div class="col-lg-6 achievement">
                        <div class="achievementinner">
                            <h3>236</h3>
                            <i class="icon-globe"></i>
                        </div>
                        <h5>Places Worldwide</h5>
                    </div>
                </article>
            </section>
        </div>
    </div>

    <div class="paralax2">
        <section class="bxwrap">
            <h4 class="testimonialsheadline">Testimonials</h4>
            <ul id="bxslider">
                <li>
                    <div class="textbx">
                        <p class="subtitle1">John Doe / General Manager</p>
                        <p class="subtitle2">Lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor
                        </p>
                    </div> <!--/textbx end-->
                </li>
                <li>
                    <div class="textbx">
                        <p class="subtitle1">Jane Doe / Designer</p>
                        <p class="subtitle2">Lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor
                        </p>
                    </div> <!--/textbx end-->
                </li>
                <li>
                    <div class="textbx">
                        <div class="textbx">
                            <p class="subtitle1">John Doe / Developer</p>
                            <p class="subtitle2">Lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor
                            </p> </div> <!--/textbx end-->
                    </div> <!--/textbx end-->
                </li>
            </ul>
        </section>
    </div><!--/paralax2 end-->
</div><!--/section1 end-->

<div class="wrapper100percent section2">
    <div id="2" class="sectionwrapper"></div>

    <div class="mainheadlinewrapper">
        <div class="mainheadline">
            <section class="cbp-so-section">
                <article class="cbp-so-side2 cbp-so-side-left">
                    <h2> <span> our </span>Team</h2>
                </article>
                <article class="cbp-so-side2 cbp-so-side-right">
                    <h4>Lorem ipsum dolor sit ame</h4>
                </article>
            </section>
        </div>
    </div>

    <div class="container">
        <section class="cbp-so-section row">
            <article class="cbp-so-side cbp-so-side-left">
                <div class="teambackground">
                    <img src="<?php echo ipThemeUrl('images/team1.jpg') ?>" alt="picture"/>
                    <h3><span>Jane Doe</span></h3>
                    <h5><span>SEO/General Manager</span></h5>
                    <p>
                        Nam liber tempor cum soluta nobis option congue imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores
                    </p>
                    <div class="additionalicons">
                        <a href=""><i class="icon-facebook"></i></a>
                        <a href=""><i class="icon-twitter"></i></a>
                        <a href=""><i class="icon-linkedin"></i></a>
                        <a href=""><i class="icon-pinterest"></i></a>
                    </div>
                </div>
            </article>

            <article class="cbp-so-side cbp-so-side-right">
                <div class="teambackground">
                    <img src="<?php echo ipThemeUrl('images/team2.jpg') ?>" alt="picture"/>
                    <h3><span>John Smith</span></h3>
                    <h5><span>Developer</span></h5>
                    <p>
                        Nam liber tempor cum soluta nobis option congue imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores
                    </p>
                    <div class="additionalicons">
                        <a href=""><i class="icon-facebook"></i></a>
                        <a href=""><i class="icon-twitter"></i></a>
                        <a href=""><i class="icon-linkedin"></i></a>
                        <a href=""><i class="icon-pinterest"></i></a>
                    </div>
                </div>
            </article>
        </section>
    </div>
</div>

<div class="mainheadlinewrappersmall">
    <div class="mainheadlinesmall">
        <section class="cbp-so-section">
            <article class="cbp-so-side2 cbp-so-side-left">
                <h2> Team's Skills</h2>
            </article>
            <article class="cbp-so-side2 cbp-so-side-right">
                <h4>Lorem ipsum dolor sit amet lorem</h4>
            </article>
        </section>
    </div>
</div>

<div class="teamskills">
    <div class="container">
        <div class="row">
            <section class="cbp-so-section">
                <article class="cbp-so-side  cbp-so-side-left">
                    <div class="wrap">
                        <div class="col-lg-6">
                            <p>HTML3</p>
                            <div class="progress-radial progress-90">
                                <div class="overlay"><p><span>90%</span></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="wrap">
                        <div class="col-lg-6">
                            <p>CSS3</p>
                            <div class="progress-radial progress-80">
                                <div class="overlay"><p><span>80%</span></p></div>
                            </div>
                        </div>
                    </div>
                </article>

                <article class="cbp-so-side  cbp-so-side-right">
                    <div class="wrap">
                        <div class="col-lg-6">
                            <p>jQuery</p>
                            <div class="progress-radial progress-50">
                                <div class="overlay"><p><span>50%</span></p></div>
                            </div>
                        </div>
                    </div>

                    <div class="wrap">
                        <div class="col-lg-6">
                            <p>SEO</p>
                            <div class="progress-radial progress-40">
                                <div class="overlay"><p><span>40%</span></p></div>
                            </div>
                        </div>
                    </div>
                </article>
            </section>
        </div>
    </div>
</div>

<div class="paralax3">
    <section class="bxwrap1">
        <h4 class="testimonialsheadline">Our Clients</h4>
        <ul id="bxslider1">
            <li>
                <a href=""> <img src="<?php echo ipThemeUrl('images/client1.png') ?>" alt="picture"/></a>
                <a href=""> <img src="<?php echo ipThemeUrl('images/client1.png') ?>" alt="picture"/></a>
                <a href=""> <img src="<?php echo ipThemeUrl('images/client1.png') ?>" alt="picture"/></a>
                <a href=""> <img src="<?php echo ipThemeUrl('images/client1.png') ?>" alt="picture"/></a>
            </li>
            <li>
                <a href=""> <img src="<?php echo ipThemeUrl('images/client1.png') ?>" alt="picture"/></a>
                <a href=""> <img src="<?php echo ipThemeUrl('images/client1.png') ?>" alt="picture"/></a>
                <a href=""> <img src="<?php echo ipThemeUrl('images/client1.png') ?>" alt="picture"/></a>
                <a href=""> <img src="<?php echo ipThemeUrl('images/client1.png') ?>" alt="picture"/></a>
            </li>
            <li>
                <a href=""> <img src="<?php echo ipThemeUrl('images/client1.png') ?>" alt="picture"/></a>
                <a href=""> <img src="<?php echo ipThemeUrl('images/client1.png') ?>" alt="picture"/></a>
                <a href=""> <img src="<?php echo ipThemeUrl('images/client1.png') ?>" alt="picture"/></a>
                <a href=""> <img src="<?php echo ipThemeUrl('images/client1.png') ?>" alt="picture"/></a>
            </li>
        </ul>
    </section>
</div><!--/paralax3 end-->
<!--/section2 end-->

<div class="wrapper100percent section3">
    <div id="3" class="sectionwrapper"></div>

    <div class="mainheadlinewrapper">
        <div class="mainheadline">
            <section class="cbp-so-section">
                <article class="cbp-so-side2 cbp-so-side-left">
                    <h2> <span> our </span>Services</h2>
                </article>
                <article class="cbp-so-side2 cbp-so-side-right">
                    <h4>Lorem ipsum dolor sit amet lorem ame</h4>
                </article>
            </section>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <section class="cbp-so-section cbp-so-side-left servicesmarginbottom">
                <article class="cbp-so-side2">
                    <div class="col-lg-4">
                        <div class="servicecolumn">
                            <div class="servicecolumninner">
                                <div class="mainicon">
                                    <i class="icon-pencil smaller" data-angle="SE"> </i>
                                </div>
                                <h3><span>Service White</span></h3>
                                <ul>
                                    <li>
                                        <i class="icon-circle"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                    <li>
                                        <i class="icon-circle"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                    <li>
                                        <i class="icon-circle"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="servicecolumn">
                            <div class="servicecolumninner">
                                <div class="mainicon">
                                    <i class="icon-glass smaller"> </i>
                                </div>
                                <h3><span>Service Purple</span></h3>
                                <ul>
                                    <li>
                                        <i class="icon-ok"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                    <li>
                                        <i class="icon-ok"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                    <li>
                                        <i class="icon-ok"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="servicecolumn">
                            <div class="servicecolumninner">
                                <div class="mainicon">
                                    <i class="icon-random smaller"> </i>
                                </div>
                                <h3><span>Service Black</span></h3>
                                <ul>
                                    <li>
                                        <i class="icon-circle-blank"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                    <li>
                                        <i class="icon-circle-blank"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                    <li>
                                        <i class="icon-circle-blank"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </article>
            </section>
        </div>

        <section class="cbp-so-section">
            <article class="cbp-so-side2 cbp-so-side-right servicesmarginbottom1">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="servicecolumn">
                            <div class="servicecolumninner">
                                <div class="mainicon">
                                    <i class="icon-cogs smaller"> </i>
                                </div>
                                <h3><span>Service Red</span></h3>
                                <ul>
                                    <li>
                                        <i class="icon-circle"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                    <li>
                                        <i class="icon-circle"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                    <li>
                                        <i class="icon-circle"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="servicecolumn">
                            <div class="servicecolumninner">
                                <div class="mainicon">
                                    <i class="icon-briefcase smaller"> </i>
                                </div>
                                <h3><span>Service Green</span></h3>
                                <ul>
                                    <li>
                                        <i class="icon-ok"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                    <li>
                                        <i class="icon-ok"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                    <li>
                                        <i class="icon-ok"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="servicecolumn">
                            <div class="servicecolumninner">
                                <div class="mainicon">
                                    <i class="icon-truck smaller"> </i>
                                </div>
                                <h3><span>Service Blue</span></h3>
                                <ul>
                                    <li>
                                        <i class="icon-circle-blank"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                    <li>
                                        <i class="icon-circle-blank"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                    <li>
                                        <i class="icon-circle-blank"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </section>
    </div>

    <div class="container">
        <div class="calltoactionwrapper">
            <div class="row">
                <div class="calltoaction col-lg-6 centered">
                    <div class="calltoactioninner">
                        <i class="icon-heart"></i>
                        <h4>This is call to action box</h4>
                        <a href="">see more</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="paralax4">
        <section class="bxwrap2">
            <h4 class="testimonialsheadline">Photostream</h4>
            <ul>
                <li>
                    <a href=""> <img src="<?php echo ipThemeUrl('images/photostream1.jpg') ?>" alt="picture"/></a>
                    <a href=""> <img src="<?php echo ipThemeUrl('images/photostream2.jpg') ?>" alt="picture"/></a>
                    <a href=""> <img src="<?php echo ipThemeUrl('images/photostream1.jpg') ?>" alt="picture"/></a>
                    <a href=""> <img src="<?php echo ipThemeUrl('images/photostream2.jpg') ?>" alt="picture"/></a>
                    <a href=""> <img src="<?php echo ipThemeUrl('images/photostream1.jpg') ?>" alt="picture"/></a>
                    <a href=""> <img src="<?php echo ipThemeUrl('images/photostream2.jpg') ?>" alt="picture"/></a>
                </li>
            </ul>
        </section>
    </div>
</div><!--/section3 end-->

<div class="wrapper100percent section4">
    <div id="4" class="sectionwrapper"></div>

    <div class="mainheadlinewrapper">
        <div class="mainheadline">
            <section class="cbp-so-section">
                <article class="cbp-so-side2 cbp-so-side-left">
                    <h2> <span> our </span>Portfolio</h2>
                </article>
                <article class="cbp-so-side2 cbp-so-side-right">
                    <h4>Lorem ipsum dolor sit amet lorem ipsum ame</h4>
                </article>
            </section>
        </div>
    </div>

    <div class="container isotopecontainer">
        <div class="row">
            <div class="col-lg-12 marginbottomisotope">
                <div id="options" class="clearfix">
                    <ul id="filters" class="option-set clearfix" data-option-key="filter">
                        <li><a href="#filter" data-option-value="*" class="selected">show all</a></li>
                        <li><a href="#filter" data-option-value=".filterone">Filter One</a></li>
                        <li><a href="#filter" data-option-value=".filtertwo">Filter Two</a></li>
                    </ul>

                    <div id="container" class="photos clearfix">
                        <div class="photo photoheight1 element filtertwo">
                            <a class='iframe group4' href="additionalpage_blue.html" title="">
                                <div class="gallery-item-mask">
                                    <div class="gallery-text-down view view-first">
                                        <img src="<?php echo ipThemeUrl('images/isotope1.jpg') ?>" alt="picture"/>
                                        <div class="mask">
                                            <h2>Project Blue</h2>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <div class="photo photoheight2 element filterone">
                            <a class='iframe group4' href="additionalpage_red.html" title="">
                                <div class="gallery-item-mask">
                                    <div class="gallery-text-down view view-first">
                                        <img src="<?php echo ipThemeUrl('images/isotope2.jpg') ?>" alt="picture"/>
                                        <div class="mask">
                                            <h2>Project Red</h2>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <div class="photo photoheight3 element filtertwo">
                            <a class='iframe group4' href="additionalpage_blue.html" title="">
                                <div class="gallery-item-mask">
                                    <div class="gallery-text-down view view-first">
                                        <img src="<?php echo ipThemeUrl('images/isotope3.jpg') ?>" alt="picture"/>
                                        <div class="mask">
                                            <h2>Project Blue</h2>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <div class="photo photoheight4 element filterone">
                            <a class='iframe group4' href="additionalpage_red.html" title="">
                                <div class="gallery-item-mask">
                                    <div class="gallery-text-down view view-first">
                                        <img src="<?php echo ipThemeUrl('images/isotope4.jpg') ?>" alt="picture"/>
                                        <div class="mask">
                                            <h2>Project Red</h2>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>


                        <div class="photo photoheight5 element filtertwo">
                            <a class='iframe group4' href="additionalpage_blue.html" title="">
                                <div class="gallery-item-mask">
                                    <div class="gallery-text-down view view-first">
                                        <img src="<?php echo ipThemeUrl('images/isotope5.jpg') ?>" alt="picture"/>
                                        <div class="mask">
                                            <h2>Project Blue</h2>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <div class="photo photoheight6 element filterone">
                            <a class='iframe group4' href="additionalpage_red.html" title="">
                                <div class="gallery-item-mask">
                                    <div class="gallery-text-down view view-first">
                                        <img src="<?php echo ipThemeUrl('images/isotope6.jpg') ?>" alt="picture"/>
                                        <div class="mask">
                                            <h2>Project Red</h2>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <div class="photo photoheight7 element filtertwo">
                            <a class='iframe group4' href="additionalpage_blue.html" title="">
                                <div class="gallery-item-mask">
                                    <div class="gallery-text-down view view-first">
                                        <img src="<?php echo ipThemeUrl('images/isotope7.jpg') ?>" alt="picture"/>
                                        <div class="mask">
                                            <h2>Project Blue</h2>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <div class="photo photoheight8 element filterone">
                            <a class='iframe group4' href="additionalpage_red.html" title="">
                                <div class="gallery-item-mask">
                                    <div class="gallery-text-down view view-first">
                                        <img src="<?php echo ipThemeUrl('images/isotope8.jpg') ?>" alt="picture"/>
                                        <div class="mask">
                                            <h2>Project Red</h2>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <div class="photo photoheight9 element filtertwo">
                            <a class='iframe group4' href="additionalpage_blue.html" title="">
                                <div class="gallery-item-mask">
                                    <div class="gallery-text-down view view-first">
                                        <img src="<?php echo ipThemeUrl('images/isotope9.jpg') ?>" alt="picture"/>
                                        <div class="mask">
                                            <h2>Project Blue</h2>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <div class="photo photoheight10 element filterone">
                            <a class='iframe group4' href="additionalpage_red.html" title="">
                                <div class="gallery-item-mask">
                                    <div class="gallery-text-down view view-first">
                                        <img src="<?php echo ipThemeUrl('images/isotope10.jpg') ?>" alt="picture"/>
                                        <div class="mask">
                                            <h2>Project Red</h2>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>


                        <div class="photo photoheight8 element filtertwo">
                            <a class='iframe group4' href="additionalpage_blue.html" title="">
                                <div class="gallery-item-mask">
                                    <div class="gallery-text-down view view-first">
                                        <img src="<?php echo ipThemeUrl('images/isotope8.jpg') ?>" alt="picture"/>
                                        <div class="mask">
                                            <h2>Project Blue</h2>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <div class="photo photoheight9 element filterone">
                            <a class='iframe group4' href="additionalpage_blue.html" title="">
                                <div class="gallery-item-mask">
                                    <div class="gallery-text-down view view-first">
                                        <img src="<?php echo ipThemeUrl('images/isotope9.jpg') ?>" alt="picture"/>
                                        <div class="mask">
                                            <h2>Project Red</h2>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mainheadlinewrappersmall">
        <div class="mainheadlinesmall">
            <section class="cbp-so-section">
                <article class="cbp-so-side2 cbp-so-side-left">
                    <h2>Send us message</h2>
                </article>
                <article class="cbp-so-side2 cbp-so-side-right">
                    <h4>Lorem ipsum dolor sit amet lorem amet lorem</h4>
                </article>
            </section>
        </div>
    </div>

    <div class="wrapper100percent">
        <section class="cbp-so-section">
            <article class="cbp-so-side2 cbp-so-side-right secondlinkdown">
                <a href="#6"><div class="toplink"><i class="icon-chevron-down"></i></div></a>
            </article>
        </section>
    </div>
</div><!--/section4 end-->

<div class="paralax5">
    <div id="owl-demo1" class="owl-carousel owl-theme">
        <div class="item">
            <a class=''  href="" title="">
                <div class="gallery-item-mask">
                    <div class="gallery-text-down">
                        <img src="<?php echo ipThemeUrl('images/carousel1.jpg') ?>" alt="picture"/>
                    </div>
                </div>
            </a>
        </div>

        <div class="item">
            <a class=''  href="" title="">
                <div class="gallery-item-mask">
                    <div class="gallery-text-down">
                        <img src="<?php echo ipThemeUrl('images/carousel2.jpg') ?>" alt="picture"/>
                    </div>
                </div>
            </a>
        </div>

        <div class="item">
            <a class=''  href="" title="">
                <div class="gallery-item-mask">
                    <div class="gallery-text-down">
                        <img src="<?php echo ipThemeUrl('images/carousel3.jpg') ?>" alt="picture"/>
                    </div>
                </div>
            </a>
        </div>

        <div class="item">
            <a class=''  href="" title="">
                <div class="gallery-item-mask">
                    <div class="gallery-text-down">
                        <img src="<?php echo ipThemeUrl('images/carousel4.jpg') ?>" alt="picture"/>
                    </div>
                </div>
            </a>
        </div>

        <div class="item">
            <a class=''  href="" title="">
                <div class="gallery-item-mask">
                    <div class="gallery-text-down">
                        <img src="<?php echo ipThemeUrl('images/carousel5.jpg') ?>" alt="picture"/>
                    </div>
                </div>
            </a>
        </div>

        <div class="item">
            <a class=''  href="" title="">
                <div class="gallery-item-mask">
                    <div class="gallery-text-down">
                        <img src="<?php echo ipThemeUrl('images/carousel6.jpg') ?>" alt="picture"/>
                    </div>
                </div>
            </a>
        </div>

        <div class="item">
            <a class=''  href="" title="">
                <div class="gallery-item-mask">
                    <div class="gallery-text-down">
                        <img src="<?php echo ipThemeUrl('images/carousel1.jpg') ?>" alt="picture"/>
                    </div>
                </div>
            </a>
        </div>

        <div class="item">
            <a class=''  href="" title="">
                <div class="gallery-item-mask">
                    <div class="gallery-text-down">
                        <img src="<?php echo ipThemeUrl('images/carousel2.jpg') ?>" alt="picture"/>
                    </div>
                </div>
            </a>
        </div>

        <div class="item">
            <a class=''  href="" title="">
                <div class="gallery-item-mask">
                    <div class="gallery-text-down">
                        <img src="<?php echo ipThemeUrl('images/carousel3.jpg') ?>" alt="picture"/>
                    </div>
                </div>
            </a>
        </div>

        <div class="item">
            <a class=''  href="" title="">
                <div class="gallery-item-mask">
                    <div class="gallery-text-down">
                        <img src="<?php echo ipThemeUrl('images/carousel4.jpg') ?>" alt="picture"/>
                    </div>
                </div>
            </a>
        </div>

        <div class="item">
            <a class=''  href="" title="">
                <div class="gallery-item-mask">
                    <div class="gallery-text-down">
                        <img src="<?php echo ipThemeUrl('images/carousel5.jpg') ?>" alt="picture"/>
                    </div>
                </div>
            </a>
        </div>

        <div class="item">
            <a class=''  href="" title="">
                <div class="gallery-item-mask">
                    <div class="gallery-text-down">
                        <img src="<?php echo ipThemeUrl('images/carousel6.jpg') ?>" alt="picture"/>
                    </div>
                </div>
            </a>
        </div>
    </div>

    <div id="owl-demo2" class="owl-carousel owl-theme">
        <div class="item">
            <a class=''  href="" title="">
                <div class="gallery-item-mask">
                    <div class="gallery-text-down">
                        <img src="<?php echo ipThemeUrl('images/carousel1.jpg') ?>" alt="picture"/>
                    </div>
                </div>
            </a>
        </div>

        <div class="item">
            <a class=''  href="" title="">
                <div class="gallery-item-mask">
                    <div class="gallery-text-down">
                        <img src="<?php echo ipThemeUrl('images/carousel2.jpg') ?>" alt="picture"/>
                    </div>
                </div>
            </a>
        </div>

        <div class="item">
            <a class=''  href="" title="">
                <div class="gallery-item-mask">
                    <div class="gallery-text-down">
                        <img src="<?php echo ipThemeUrl('images/carousel3.jpg') ?>" alt="picture"/>
                    </div>
                </div>
            </a>
        </div>

        <div class="item">
            <a class=''  href="" title="">
                <div class="gallery-item-mask">
                    <div class="gallery-text-down">
                        <img src="<?php echo ipThemeUrl('images/carousel4.jpg') ?>" alt="picture"/>
                    </div>
                </div>
            </a>
        </div>

        <div class="item">
            <a class=''  href="" title="">
                <div class="gallery-item-mask">
                    <div class="gallery-text-down">
                        <img src="<?php echo ipThemeUrl('images/carousel5.jpg') ?>" alt="picture"/>
                    </div>
                </div>
            </a>
        </div>

        <div class="item">
            <a class=''  href="" title="">
                <div class="gallery-item-mask">
                    <div class="gallery-text-down">
                        <img src="<?php echo ipThemeUrl('images/carousel6.jpg') ?>" alt="picture"/>
                    </div>
                </div>
            </a>
        </div>

        <div class="item">
            <a class=''  href="" title="">
                <div class="gallery-item-mask">
                    <div class="gallery-text-down">
                        <img src="<?php echo ipThemeUrl('images/carousel1.jpg') ?>" alt="picture"/>
                    </div>
                </div>
            </a>
        </div>

        <div class="item">
            <a class=''  href="" title="">
                <div class="gallery-item-mask">
                    <div class="gallery-text-down">
                        <img src="<?php echo ipThemeUrl('images/carousel2.jpg') ?>" alt="picture"/>
                    </div>
                </div>
            </a>
        </div>

        <div class="item">
            <a class=''  href="" title="">
                <div class="gallery-item-mask">
                    <div class="gallery-text-down">
                        <img src="<?php echo ipThemeUrl('images/carousel3.jpg') ?>" alt="picture"/>
                    </div>
                </div>
            </a>
        </div>

        <div class="item">
            <a class=''  href="a" title="">
                <div class="gallery-item-mask">
                    <div class="gallery-text-down">
                        <img src="<?php echo ipThemeUrl('images/carousel4.jpg') ?>" alt="picture"/>
                    </div>
                </div>
            </a>
        </div>

        <div class="item">
            <a class=''  href="" title="">
                <div class="gallery-item-mask">
                    <div class="gallery-text-down">
                        <img src="<?php echo ipThemeUrl('images/carousel5.jpg') ?>" alt="picture"/>
                    </div>
                </div>
            </a>
        </div>

        <div class="item">
            <a class=''  href="" title="">
                <div class="gallery-item-mask">
                    <div class="gallery-text-down">
                        <img src="<?php echo ipThemeUrl('images/carousel6.jpg') ?>" alt="picture"/>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div><!--/paralax5 end-->

<div class="wrapper100percent section5">
    <div id="5" class="sectionwrapper"></div>

    <div class="mainheadlinewrapper">
        <div class="mainheadline">
            <section class="cbp-so-section">
                <article class="cbp-so-side2 cbp-so-side-left">
                    <h2><span> our </span>Blog</h2>
                </article>
                <article class="cbp-so-side2 cbp-so-side-right">
                    <h4>Lorem ipsum dolor amet</h4>
                </article>
            </section>
        </div>
    </div>

    <div class="wrapper100percent">
        <div  id="owl-demo3" class="owl-carousel owl-theme">
            <div class="item blogitems">
                <img src="<?php echo ipThemeUrl('images/carousel3.jpg') ?>" alt="picture"/>
                <h5>Lorem ipsum dolor</h5>
                <p class="date">25/11/2013</p>
                <p>Lorem ipsum dolor sit amet lorem lorem ipsum </p>
                <a href="" class="link">Read MOre </a>
            </div>

            <div class="item blogitems">
                <img src="<?php echo ipThemeUrl('images/carousel2.jpg') ?>" alt="picture"/>
                <h5>Lorem ipsum dolor</h5>
                <p class="date">25/11/2013</p>
                <p>Lorem ipsum dolor sit amet lorem lorem ipsum </p>
                <a href="" class="link">Read MOre </a>
            </div>

            <div class="item blogitems">
                <img src="<?php echo ipThemeUrl('images/carousel1.jpg') ?>" alt="picture"/>
                <h5>Lorem ipsum dolor</h5>
                <p class="date">25/11/2013</p>
                <p>Lorem ipsum dolor sit amet lorem lorem ipsum </p>
                <a href="" class="link">Read MOre </a>
            </div>

            <div class="item blogitems">
                <img src="<?php echo ipThemeUrl('images/carousel4.jpg') ?>" alt="picture"/>
                <h5>Lorem ipsum dolor</h5>
                <p class="date">25/11/2013</p>
                <p>Lorem ipsum dolor sit amet lorem lorem ipsum </p>
                <a href="" class="link">Read MOre </a>
            </div>

            <div class="item blogitems">
                <img src="<?php echo ipThemeUrl('images/carousel3.jpg') ?>" alt="picture"/>
                <h5>Lorem ipsum dolor</h5>
                <p class="date">25/11/2013</p>
                <p>Lorem ipsum dolor sit amet lorem lorem ipsum </p>
                <a href="" class="link">Read MOre </a>
            </div>

            <div class="item blogitems">
                <img src="<?php echo ipThemeUrl('images/carousel4.jpg') ?>" alt="picture"/>
                <h5>Lorem ipsum dolor</h5>
                <p class="date">25/11/2013</p>
                <p>Lorem ipsum dolor sit amet lorem lorem ipsum </p>
                <a href="" class="link">Read MOre </a>
            </div>

            <div class="item blogitems">
                <img src="<?php echo ipThemeUrl('images/carousel2.jpg') ?>" alt="picture"/>
                <h5>Lorem ipsum dolor</h5>
                <p class="date">25/11/2013</p>
                <p>Lorem ipsum dolor sit amet lorem lorem ipsum </p>
                <a href="" class="link">Read MOre </a>
            </div>

            <div class="item blogitems">
                <img src="<?php echo ipThemeUrl('images/carousel1.jpg') ?>" alt="picture"/>
                <h5>Lorem ipsum dolor</h5>
                <p class="date">25/11/2013</p>
                <p>Lorem ipsum dolor sit amet lorem lorem ipsum </p>
                <a href="" class="link">Read MOre </a>
            </div>
        </div>
    </div>

    <div class="paralax6">
    </div>
</div><!--/section6 end-->

<div class="footerbottom">
    <div id="6" class="sectionwrapper"></div>

    <div class="mainheadlinewrapper">
        <div class="mainheadline">
            <section class="cbp-so-section">
                <article class="cbp-so-side2 cbp-so-side-left">
                    <h2>Contact us</h2>
                </article>
                <article class="cbp-so-side2 cbp-so-side-right">
                    <h4>Lorem ipsum dolor sit amet lorem amet</h4>
                </article>
            </section>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h5><i class="icon-envelope"></i> Your Message...</h5>

                <!--/comment-form-->
                <div>
                    <form method="post" action="sendEmail.php">
                        <div>
                            <div id="main">
                                <div>
                                    <div class="">
                                        <div class="contact1">
                                            <p><input type="text" placeholder="your name" name="name" id="name" class="commentfield" /></p>
                                        </div>
                                        <div class="contact2">
                                            <p> <input type="text" placeholder="your e-mail adress" name="email" id="email" class="commentfield" /></p>
                                        </div>
                                        <p>
                                            <textarea name="comments" placeholder="message" id="comments" rows="12" cols="5" class="textarea"></textarea>
                                        </p>
                                        <div class="buttoncontactwrapper">
                                            <p><input type="submit"  name="submit"  class="buttoncontact" id="submit" value="Siųsti"/></p>
                                        </div>
                                        <ul id="response"></ul>
                                    </div>
                                </div>
                            </div></div>
                    </form>
                    <!--/comment-form-end-->

                    <div class="row address">
                        <div class="col-lg-4">
                            <h6>Address:</h6>
                            <p>Office One: Your Street Name 123, Your City</p>
                            <p>Office Two: Your Street Name 123, Your City</p>
                        </div>
                        <div class="col-lg-4">
                            <h6>Phone:</h6>
                            <p>Phone One: 111 - 111-1111 (08am - 04pm) </p>
                            <p>Phone Two: 222 - 222-2222 (08am - 04pm) </p>
                        </div>
                        <div class="col-lg-4">
                            <h6>E-mail:</h6>
                            <p>E-mail One: <a href="mailto:example@example.com">example@example.com</a></p>
                            <p>E-mail Two:  <a href="mailto:example@example.com">example@example.com</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo ipView('_footer.php')->render(); ?>