<?php echo ipView('_page_header.php')->render(); ?>


<div class="wrapper100percent section1">
    <div id="2" class="sectionwrapper"></div>

    <div class="mainheadlinewrapper">
        <div class="mainheadline paralax7">
            <section class="cbp-so-section">
                <article class="cbp-so-side2 cbp-so-side-left">
                    <h2>

                        <?php
                        echo ipSlot('text', array(
                            'id' => 'TitleOrange',
                            'default' => 'Pavadinimas',
                        ));
                        ?>
                    </h2>
                </article>

            </section>
        </div>
    </div>

    <div class="container">
        <section class="cbp-so-section row">

            <div class="col-lg-12">
                <article class="cbp-so-side-left">
                    <div class="teambackground">
                        <?php echo ipBlock('main')->render(); ?>
                    </div>
                </article>
            </div>


        </section>
    </div>
</div>

<!--/section2 end-->

<?php echo ipView('_page_footer.php')->render(); ?>



