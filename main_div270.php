
,<div class="wrapper100percent section2">
    <div id="2" class="sectionwrapper"></div>

    <div class="mainheadlinewrapper">
        <div class="mainheadline">
            <section class="cbp-so-section">
                <article class="cbp-so-side2 cbp-so-side-left">
                    <h2>
                        <?php
                        echo ipSlot('text', array(
                            'id' => 'Text5',
                            'tag' => 'span',
                            'default' => 'mūsų',
                        ));
                        ?>
                        <?php
                        echo ipSlot('text', array(
                            'id' => 'Team',
                            'default' => 'komanda',
                        ));
                        ?>
                    </h2>
                </article>
                <article class="cbp-so-side2 cbp-so-side-right">
                    <h4>
                        <?php
                        echo ipSlot('text', array(
                            'id' => 'TeamText',
                            'default' => 'Dirbame Jums',
                        ));
                        ?>
                    </h4>
                </article>
            </section>
        </div>
    </div>

    <div class="container">
        <section class="cbp-so-section row">
            <div class="col-lg-3">
                <article class="cbp-so-side-left">
                    <div class="teambackground">
                        <img src="<?php echo ipThemeUrl('images/team1.jpg') ?>" alt="picture"/>
                        <h3>
                            <?php
                            echo ipSlot('text', array(
                                'id' => 'Team1',
                                'tag' => 'span',
                                'default' => 'Ieva Petrokaitė',
                            ));
                            ?>
                        </h3>
                        <h5>
                            <?php
                            echo ipSlot('text', array(
                                'id' => 'TeamText1',
                                'tag' => 'span',
                                'default' => 'Vadovė',
                            ));
                            ?>
                        </h5>
                        <p>
                            <?php
                            echo ipSlot('text', array(
                                'id' => 'TeamTextAll1',
                                'default' => 'Viskas apie Ievą...',
                            ));
                            ?>
                        </p>
                    </div>
                </article>
            </div>

            <div class="col-lg-3">
                <article class="cbp-so-side-left">
                    <div class="teambackground">
                        <img src="<?php echo ipThemeUrl('images/team2.jpg') ?>" alt="picture"/>
                        <h3><span>John Smith</span></h3>
                        <h5><span>Developer</span></h5>
                        <p>
                            Nam liber tempor cum soluta nobis option congue imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores
                        </p>
                        <div class="additionalicons">
                            <a href=""><i class="icon-facebook"></i></a>
                            <a href=""><i class="icon-twitter"></i></a>
                            <a href=""><i class="icon-linkedin"></i></a>
                            <a href=""><i class="icon-pinterest"></i></a>
                        </div>
                    </div>
                </article>
            </div>

            <div class="col-lg-3">
                <article class="cbp-so-side-right">
                    <div class="teambackground">
                        <img src="<?php echo ipThemeUrl('images/team1.jpg') ?>" alt="picture"/>
                        <h3><span>Jane Doe</span></h3>
                        <h5><span>SEO/General Manager</span></h5>
                        <p>
                            Nam liber tempor cum soluta nobis option congue imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores
                        </p>
                        <div class="additionalicons">
                            <a href=""><i class="icon-facebook"></i></a>
                            <a href=""><i class="icon-twitter"></i></a>
                            <a href=""><i class="icon-linkedin"></i></a>
                            <a href=""><i class="icon-pinterest"></i></a>
                        </div>
                    </div>
                </article>
            </div>

            <div class="col-lg-3">
                <article class="cbp-so-side-right">
                    <div class="teambackground">
                        <img src="<?php echo ipThemeUrl('images/team2.jpg') ?>" alt="picture"/>
                        <h3><span>John Smith</span></h3>
                        <h5><span>Developer</span></h5>
                        <p>
                            Nam liber tempor cum soluta nobis option congue imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores
                        </p>
                        <div class="additionalicons">
                            <a href=""><i class="icon-facebook"></i></a>
                            <a href=""><i class="icon-twitter"></i></a>
                            <a href=""><i class="icon-linkedin"></i></a>
                            <a href=""><i class="icon-pinterest"></i></a>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>
</div>

<!--/section2 end-->