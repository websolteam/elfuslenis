<div class = "wrapper100percent section3">
    <div id = "3" class = "sectionwrapper"></div>

    <div class = "mainheadlinewrapper">
        <div class = "mainheadline">
            <section class = "cbp-so-section">
                <article class = "cbp-so-side2 cbp-so-side-left">
                    <h2> <span> our </span>Services</h2>
                </article>
                <article class = "cbp-so-side2 cbp-so-side-right">
                    <h4>Lorem ipsum dolor sit amet lorem ame</h4>
                </article>
            </section>
        </div>
    </div>

    <div class = "container">
        <div class = "row">
            <section class = "cbp-so-section cbp-so-side-left servicesmarginbottom">
                <article class = "cbp-so-side2">
                    <div class = "col-lg-4">
                        <div class = "servicecolumn">
                            <div class = "servicecolumninner">
                                <div class = "mainicon">
                                    <i class = "icon-pencil smaller" data-angle = "SE"> </i>
                                </div>
                                <h3><span>Service White</span></h3>
                                <ul>
                                    <li>
                                        <i class = "icon-circle"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                    <li>
                                        <i class = "icon-circle"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                    <li>
                                        <i class = "icon-circle"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class = "col-lg-4">
                        <div class = "servicecolumn">
                            <div class = "servicecolumninner">
                                <div class = "mainicon">
                                    <i class = "icon-glass smaller"> </i>
                                </div>
                                <h3><span>Service Purple</span></h3>
                                <ul>
                                    <li>
                                        <i class = "icon-ok"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                    <li>
                                        <i class = "icon-ok"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                    <li>
                                        <i class = "icon-ok"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class = "col-lg-4">
                        <div class = "servicecolumn">
                            <div class = "servicecolumninner">
                                <div class = "mainicon">
                                    <i class = "icon-random smaller"> </i>
                                </div>
                                <h3><span>Service Black</span></h3>
                                <ul>
                                    <li>
                                        <i class = "icon-circle-blank"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                    <li>
                                        <i class = "icon-circle-blank"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                    <li>
                                        <i class = "icon-circle-blank"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </article>
            </section>
        </div>

        <section class = "cbp-so-section">
            <article class = "cbp-so-side2 cbp-so-side-right servicesmarginbottom1">
                <div class = "row">
                    <div class = "col-lg-4">
                        <div class = "servicecolumn">
                            <div class = "servicecolumninner">
                                <div class = "mainicon">
                                    <i class = "icon-cogs smaller"> </i>
                                </div>
                                <h3><span>Service Red</span></h3>
                                <ul>
                                    <li>
                                        <i class = "icon-circle"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                    <li>
                                        <i class = "icon-circle"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                    <li>
                                        <i class = "icon-circle"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class = "col-lg-4">
                        <div class = "servicecolumn">
                            <div class = "servicecolumninner">
                                <div class = "mainicon">
                                    <i class = "icon-briefcase smaller"> </i>
                                </div>
                                <h3><span>Service Green</span></h3>
                                <ul>
                                    <li>
                                        <i class = "icon-ok"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                    <li>
                                        <i class = "icon-ok"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                    <li>
                                        <i class = "icon-ok"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class = "col-lg-4">
                        <div class = "servicecolumn">
                            <div class = "servicecolumninner">
                                <div class = "mainicon">
                                    <i class = "icon-truck smaller"> </i>
                                </div>
                                <h3><span>Service Blue</span></h3>
                                <ul>
                                    <li>
                                        <i class = "icon-circle-blank"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                    <li>
                                        <i class = "icon-circle-blank"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                    <li>
                                        <i class = "icon-circle-blank"></i>
                                        Typi non habent claritatem insitam
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </section>
    </div>

    <div class = "container">
        <div class = "calltoactionwrapper">
            <div class = "row">
                <div class = "calltoaction col-lg-6 centered">
                    <div class = "calltoactioninner">
                        <i class = "icon-heart"></i>
                        <h4>This is call to action box</h4>
                        <a href = "">see more</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class = "paralax4">
        <section class = "bxwrap2">
            <h4 class = "testimonialsheadline">Photostream</h4>
            <ul>
                <li>
                    <a href = ""> <img src = "<?php echo ipThemeUrl('images/photostream1.jpg') ?>" alt = "picture"/></a>
                    <a href = ""> <img src = "<?php echo ipThemeUrl('images/photostream2.jpg') ?>" alt = "picture"/></a>
                    <a href = ""> <img src = "<?php echo ipThemeUrl('images/photostream1.jpg') ?>" alt = "picture"/></a>
                    <a href = ""> <img src = "<?php echo ipThemeUrl('images/photostream2.jpg') ?>" alt = "picture"/></a>
                    <a href = ""> <img src = "<?php echo ipThemeUrl('images/photostream1.jpg') ?>" alt = "picture"/></a>
                    <a href = ""> <img src = "<?php echo ipThemeUrl('images/photostream2.jpg') ?>" alt = "picture"/></a>
                </li>
            </ul>
        </section>
    </div>
</div><!--/section3 end-->