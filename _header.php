<?php echo ipDoctypeDeclaration(); ?>
<html <?php echo ipHtmlAttributes(); ?>>
    <head>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,800" rel="stylesheet" type="text/css">
        <?php
        ipAddCss('assets/theme.css');
        echo ipHead();
        ?>

        <link href='http://fonts.googleapis.com/css?family=Arvo:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Shadows+Into+Light+Two&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

        <!--[if IE]>
       <link rel="stylesheet" href="css/ie8.css"/>
       <![endif]-->


        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->


        <script src="<?php echo ipThemeUrl('js/jquery.js') ?>"></script>
        <script src="<?php echo ipThemeUrl('js/jquery-migrate.js') ?>"></script>
        <script src="<?php echo ipThemeUrl('js/modernizr.custom.js') ?>"></script>

        <script>
            $(window).load(function() {
                $('#mask').delay(3000).fadeOut('slow');
            });
        </script>


        <link href="<?php echo ipThemeUrl('css/bootstrap.css') ?>" rel="stylesheet">
        <link href="<?php echo ipThemeUrl('css/style.css') ?>" rel="stylesheet">
        <link href="<?php echo ipThemeUrl('css/media.css') ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo ipThemeUrl('css/font-awesome.css') ?>">
        <link href="<?php echo ipThemeUrl('css/owl.carousel.css') ?>" rel="stylesheet">
        <link href="<?php echo ipThemeUrl('css/owl.theme.css') ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo ipThemeUrl('css/colorbox.css') ?>" />
        <link rel="stylesheet" href="<?php echo ipThemeUrl('css/animate.css') ?>" />
        <link href="<?php echo ipThemeUrl('css/bxslider.css') ?>" rel="stylesheet" />

        <style>
            body{
                background:#fff url(<?php echo ipThemeUrl('images/paralax7.jpg') ?>) top center no-repeat fixed;
            }
        </style>

    </head>
    <body>

        <div id="0"></div>

        <header class="esTheme">
            <div id="nav">

                <nav class="navbar menudesktopcolor navbar-default navbar-fixed-top" role="navigation">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                            <span class="sr-only"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/index.php">
                            <img src="<?php echo ipThemeUrl('images/logo.png') ?>" alt="picture"/></a>

                    </div>


                    <div class="navbar-collapse navbar-ex1-collapse ">
                        <nav class="cl-effect-12 nav navbar-nav pull-right">
                            <?php echo ipSlot('menu', 'menu1'); ?>
                        </nav>
                    </div><!-- /.navbar-collapse -->
                </nav>
            </div>
            <!--/menu end-->

        </header>

