<footer class="paralax7">
    <div class="footerbottomblack">

        <div class="row">
            <div class="col-lg-6 copyright text-left">
                <p> Elfų slėnis 2014<span>&copy; Sprendimas.<a href="http://www.websol.lt">WebSol.lt</a>.</span></p>
            </div>
            <div class="col-lg-6 text-right scrollbutton">  <!--/scrollbutton-->

                <a href="javascript:scrollToTop()" title="go to top">Aukštyn<i class="icon-arrow-up"></i></a>
            </div><!--/scrollbutton end-->
        </div>

    </div>

</footer>
<!--/footerbottom end-->
<!--/footerbottom end-->

<script src="<?php echo ipThemeUrl('js/smoothscroll.js') ?>"></script>
<script src="<?php echo ipThemeUrl('js/bootstrap.js') ?>"></script>
<script src="<?php echo ipThemeUrl('js/classie.js') ?>"></script>
<script src="<?php echo ipThemeUrl('js/cbpscroller.js') ?>"></script>
<script src="<?php echo ipThemeUrl('js/jquery.bxslider.js') ?>"></script>
<script type="text/javascript" src="<?php echo ipThemeUrl('js/jquery.parallax-1.1.3.js') ?>"></script>
<script src="<?php echo ipThemeUrl('js/jquery.isotope.min.js') ?>"></script>
<script src="<?php echo ipThemeUrl('js/owl.carousel.js') ?>"></script>
<script src="<?php echo ipThemeUrl('js/jquery.colorbox.js') ?>"></script>
<script src="<?php echo ipThemeUrl('js/imagesloaded.js') ?>"></script>
<script src="<?php echo ipThemeUrl('js/jquery.bxslider.js') ?>"></script>
<script src="<?php echo ipThemeUrl('js/retina.js') ?>"></script>

<script src="<?php echo ipThemeUrl('js/scripts.js') ?>"></script>

<?php echo ipAddJs('assets/site.js'); ?>
<?php echo ipJs(); ?>


<script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-50837795-1', 'elfuslenis.lt');
    ga('send', 'pageview');

</script>

</body>
</html>

