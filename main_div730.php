<div class="wrapper100percent section5">
    <div id="5" class="sectionwrapper"></div>

    <div class="mainheadlinewrapper">
        <div class="mainheadline">
            <section class="cbp-so-section">
                <article class="cbp-so-side2 cbp-so-side-left">
                    <h2><span> our </span>Blog</h2>
                </article>
                <article class="cbp-so-side2 cbp-so-side-right">
                    <h4>Lorem ipsum dolor amet</h4>
                </article>
            </section>
        </div>
    </div>

    <div class="wrapper100percent">
        <div  id="owl-demo3" class="owl-carousel owl-theme">
            <div class="item blogitems">
                <img src="<?php echo ipThemeUrl('images/carousel3.jpg') ?>" alt="picture"/>
                <h5>Lorem ipsum dolor</h5>
                <p class="date">25/11/2013</p>
                <p>Lorem ipsum dolor sit amet lorem lorem ipsum </p>
                <a href="" class="link">Read MOre </a>
            </div>

            <div class="item blogitems">
                <img src="<?php echo ipThemeUrl('images/carousel2.jpg') ?>" alt="picture"/>
                <h5>Lorem ipsum dolor</h5>
                <p class="date">25/11/2013</p>
                <p>Lorem ipsum dolor sit amet lorem lorem ipsum </p>
                <a href="" class="link">Read MOre </a>
            </div>

            <div class="item blogitems">
                <img src="<?php echo ipThemeUrl('images/carousel1.jpg') ?>" alt="picture"/>
                <h5>Lorem ipsum dolor</h5>
                <p class="date">25/11/2013</p>
                <p>Lorem ipsum dolor sit amet lorem lorem ipsum </p>
                <a href="" class="link">Read MOre </a>
            </div>

            <div class="item blogitems">
                <img src="<?php echo ipThemeUrl('images/carousel4.jpg') ?>" alt="picture"/>
                <h5>Lorem ipsum dolor</h5>
                <p class="date">25/11/2013</p>
                <p>Lorem ipsum dolor sit amet lorem lorem ipsum </p>
                <a href="" class="link">Read MOre </a>
            </div>

            <div class="item blogitems">
                <img src="<?php echo ipThemeUrl('images/carousel3.jpg') ?>" alt="picture"/>
                <h5>Lorem ipsum dolor</h5>
                <p class="date">25/11/2013</p>
                <p>Lorem ipsum dolor sit amet lorem lorem ipsum </p>
                <a href="" class="link">Read MOre </a>
            </div>

            <div class="item blogitems">
                <img src="<?php echo ipThemeUrl('images/carousel4.jpg') ?>" alt="picture"/>
                <h5>Lorem ipsum dolor</h5>
                <p class="date">25/11/2013</p>
                <p>Lorem ipsum dolor sit amet lorem lorem ipsum </p>
                <a href="" class="link">Read MOre </a>
            </div>

            <div class="item blogitems">
                <img src="<?php echo ipThemeUrl('images/carousel2.jpg') ?>" alt="picture"/>
                <h5>Lorem ipsum dolor</h5>
                <p class="date">25/11/2013</p>
                <p>Lorem ipsum dolor sit amet lorem lorem ipsum </p>
                <a href="" class="link">Read MOre </a>
            </div>

            <div class="item blogitems">
                <img src="<?php echo ipThemeUrl('images/carousel1.jpg') ?>" alt="picture"/>
                <h5>Lorem ipsum dolor</h5>
                <p class="date">25/11/2013</p>
                <p>Lorem ipsum dolor sit amet lorem lorem ipsum </p>
                <a href="" class="link">Read MOre </a>
            </div>
        </div>
    </div>

    <div class="paralax6">
    </div>
</div><!--/section6 end-->